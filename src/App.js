
// import './App.css';
// import Clock from './component/clock'
// import Clocklist from './component/list'



// import ClickCounter from './hoc/ClickCounter'
// import HoverCounter from './hoc/HoverCounter'
// import ClickCounters from './component/randerProps/ClickCountres'
// import Counter from './component/randerProps/Counter'
// import HoverCounters from './component/randerProps/HoverCounters'

import ClickCounter from './component/hocs/ClickCounter';
import Hovercounter from './component/hocs/HoverCounter';
import ClickCountr from './component/rps/ClickCountr';
import HoverCountr from './component/rps/HoverCountr';
import WithCounter from './component/rps/WithCounter';
function App() {

  return (

    <>
        <ClickCounter/>
        <Hovercounter/>
        <WithCounter render={(click, handleClick)=>(<ClickCountr click={click} handleClick={handleClick}/>)}/>
        <WithCounter render={(click, handleClick)=>(<HoverCountr click={click} handleClick={handleClick}/>)}/>

    </>
    // <>
    // <ClickCounter/>
    // <HoverCounter/>
    // <Counter render={(count, handleClick) => (<ClickCounters count={count} handleClick={handleClick}/>)}/>
    // <Counter render={(count, handleClick) => (<HoverCounters count={count} handleClick={handleClick}/>)}/>
    // </>
  );
}
// function App() {
//   const quantities = [1,2,3];
//   return (
//     <Clocklist quantities= {quantities} />
//   );
// }

export default App;
