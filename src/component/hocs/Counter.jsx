import React, { Component } from "react";

const Counter = (OriginalComponent) => {
  class NewComponent extends Component {
    state = {
      click: 0,
    };

    handleClick = () => {
      this.setState((prevState) => ({ click: prevState.click + 1 }));
    };

    render() {
      const { click } = this.state;
      return (
        <div>
          <OriginalComponent click={click} handleClick={this.handleClick} />
        </div>
      );
    }
  }

  return NewComponent;
};

export default Counter;
