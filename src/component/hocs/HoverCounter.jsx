import Counter from './Counter';
const Hovercounter = (props) => {
  const { click, handleClick } = props;
  return (
    <p onMouseOver={handleClick}>
      hover here {click}
    </p>
  );
};

export default Counter(Hovercounter)