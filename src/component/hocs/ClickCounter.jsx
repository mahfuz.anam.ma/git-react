import Counter from "./Counter";

const ClickCounter = (props) => {
  const { click, handleClick } = props;
  return(
    <button type="button" onClick={handleClick}>
      {" "}
      click {click} time{" "}
    </button>
  );
};

export default Counter(ClickCounter);
