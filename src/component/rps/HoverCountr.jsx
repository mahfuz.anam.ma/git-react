import React from 'react'

export default function HoverCountr({click, handleClick}) {

    return (
        <div>
            <h1 onMouseOver={handleClick}> hovered {click} time</h1>
        </div>
    )
}
