import React from 'react'

export default function ClickCounter({click, handleClick}) {
    
    return (
       <button type='button' onClick={handleClick}> click {click} </button>
    )
}
